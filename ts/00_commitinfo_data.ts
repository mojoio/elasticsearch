/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@apiclient.xyz/elasticsearch',
  version: '1.0.44',
  description: 'log to elasticsearch in a kibana compatible format'
}
